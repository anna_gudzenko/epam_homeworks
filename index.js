const express = require("express");
const app = express();
const fs = require("fs");
const path = require("path");
const { logger } = require("./middleware/logevents");

const PORT = 8080;
const FILEFOLDERNAME = "files";
const FILEFOLDER = path.join(__dirname, "./", FILEFOLDERNAME);
const ENCODING = "utf-8";

const createFilesFolder = (fileFolder) => {
  try {
    if (!fs.existsSync(fileFolder)) {
      fs.mkdirSync(fileFolder);
    }
  } catch (error) {
    throw error;
  }
};

app.use(express.json());
app.use(logger);

let createFile = (req, res) => {
  createFilesFolder(FILEFOLDER);
  const fileName = req.body.filename;
  const newFileNAme = Date.now() + fileName;

  const data = req.body.content;
  const file = `${path.join(__dirname, "./", FILEFOLDERNAME)}\\${fileName}`;

  if (!fileName) {
    res.status(400).json({ message: "Please specify 'filename' parameter" });
  } else if (!data) {
    res.status(400).json({ message: "Please specify 'content' parameter" });
  } else if (fs.existsSync(file)) {
    // res.status(400).json({ message: "File exists" });
    fs.writeFile(
      `${path.join(__dirname, "./", FILEFOLDERNAME)}\\${newFileNAme}`,
      data,
      (err) => {
        if (err) {
          res.status(500).json({ message: "Server error" });
        } else {
          res.status(200).json({ message: "File created successfully" });
        }
      }
    );
  } else {
    try {
      fs.writeFile(file, data, (err) => {
        if (err) {
          res.status(500).json({ message: "Server error" });
        } else if (!req.body.content) {
          res
            .status(400)
            .json({ message: "Please specify 'content' parameter" });
        } else if (!req.body.filename.match(/\.(log|txt|json|yaml|xml|js)$/i)) {
          res
            .status(400)
            .json({ message: "Extention 'filename' is not correct" });
        } else {
          res.status(200).json({ message: "File created successfully" });
        }
      });
    } catch (error) {
      res.status(500).json({ message: "Server error " + error.message });
    }
  }
};

let getFiles = (req, res) => {
  try {
    // createFilesFolder(FILEFOLDER);
    fs.readdir(FILEFOLDER, (err, files) => {
      if (err) {
        res.status(500).json({ message: "Server error" });
      } else if (!fs.existsSync(path.join(__dirname, "./", FILEFOLDERNAME))) {
        res.status(400).json({ message: "Client error" });
      } else if (files.length === 0) {
        res.status(400).json({ message: "Files not found" });
      } else {
        res.status(200).json({ message: "Success", files: files });
      }
    });
  } catch (error) {
    res.status(500).json({ message: "Server error " + error.message });
  }
};

let getFile = (req, res) => {
  try {
    let uploadDate;
    createFilesFolder(FILEFOLDER);
    const fileName = req.params.filename;
    const file = `${path.join(__dirname, "./", FILEFOLDERNAME)}\\${fileName}`;
    if (!fileName) {
      res.status(400).json({ message: "Params 'filename' is required" });
    } else if (!fs.existsSync(file)) {
      res
        .status(400)
        .json({ message: `No file with '${fileName}' filename found` });
    } else {
      fs.stat(file, (err, stats) => {
        if (err) {
          res.status(500).json({ message: "Server error" });
        } else {
          uploadDate = stats.birthtime;

          fs.readFile(file, ENCODING, (err, data) => {
            if (err) {
              res.status(500).json({ message: "Server error" });
            } else if (!data) {
              res.status(400).json({
                message: `No contenet in filename: ${req.params.filename} `,
              });
            } else {
              res.status(200).json({
                message: "Sucsess",
                filename: fileName,
                content: data,
                extension: path.extname(fileName).substring(1),
                uploadedDate: uploadDate,
              });
            }
          });
        }
      });
    }
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

app.post("/api/files", createFile);

app.get("/api/files", getFiles);

app.get("/api/files/:filename", getFile);

app.listen(PORT, () => {
  console.log(`Server app listening on port ${PORT}`);
});
